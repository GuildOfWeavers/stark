// IMPORTS
// ================================================================================================
import * as assert from 'assert';
import { Stark, PrimeField } from '../index';
import { ExecutionFrame, EvaluationFrame, FiniteField } from 'stark';

// STARK DEFINITION
// ================================================================================================
//const modulus = 96769n;
const modulus = 2n ** 256n - 351n * 2n ** 32n + 1n;

const fibstark = new Stark({
    field               : new PrimeField(modulus),
    registerCount       : 2,
    tFunction           : fibTransition,
    tConstraints        : [fibConstraint1, fibConstraint2],
    tConstraintDegree   : 1
});

function fibTransition(frame: ExecutionFrame, field: FiniteField) {
    const v0 = frame.getValue(0);
    const v1 = frame.getValue(1);
    const nv0 = field.add(v0, v1);
    const nv1 = field.add(v1, nv0);

    frame.setNextValue(0, nv0);
    frame.setNextValue(1, nv1);
}

function fibConstraint1(frame: EvaluationFrame, field: FiniteField) {
    const v0 = frame.getValue(0);
    const v1 = frame.getValue(1);
    const nv0 = frame.getNextValue(0);
    return field.sub(nv0, field.add(v0, v1));
}

function fibConstraint2(frame: EvaluationFrame, field: FiniteField) {
    const v0 = frame.getValue(0);
    const v1 = frame.getValue(1);
    const nv0 = field.add(v0, v1);
    const nv1 = frame.getNextValue(1);
    return field.sub(nv1, field.add(v1, nv0));
}

// TESTING
// ================================================================================================
//let steps = 2**6, result = 44812n;    // small field
//let steps = 2**6, result = 10610209857723n;
let steps = 2**12, result = 104977722638811224574308232524320846113722559059396796075928848961406917560062n;
//let steps = 2**14, result = 85349894383756387438611256783735945697118188882935564131390617651912056424986n;

const inputs = [1n, 1n];
const assertions = [{ step: 0, register: 0, value: 1n }, {step: 0, register: 1, value: 1n}, { step: steps-1, register: 1, value: result }];
let proof = fibstark.prove(assertions, steps, inputs);
console.log('-'.repeat(100));

let start = Date.now();
const buf = fibstark.serialize(proof);
console.log(`Proof serialized in ${Date.now() - start} ms`);
console.log(`Proof size: ${buf.byteLength} bytes`);
assert(buf.byteLength === fibstark.sizeOf(proof));
console.log('-'.repeat(100));

start = Date.now();
proof = fibstark.parse(buf);
console.log(`Proof parsed in ${Date.now() - start} ms`);
console.log('-'.repeat(100));

fibstark.verify(assertions, proof, steps);
console.log('-'.repeat(100));