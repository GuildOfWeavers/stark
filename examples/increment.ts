// IMPORTS
// ================================================================================================
import { Stark, PrimeField } from '../index';
import { ExecutionFrame, EvaluationFrame, FiniteField, Constant, ConstantPattern } from 'stark';

// STARK DEFINITION
// ================================================================================================
const modulus = 96769n;

const incstark = new Stark({
    field               : new PrimeField(modulus),
    registerCount       : 1,
    constantCount       : 2,
    tFunction           : incTransition,
    tConstraints        : [incConstraint],
    tConstraintDegree   : 1
});

function incTransition(frame: ExecutionFrame, field: FiniteField) {
    const v0 = frame.getValue(0);
    const k0 = frame.getConst(0);
    const k1 = frame.getConst(1);
    const nv0 = field.add(field.add(field.add(v0, 1n), k0), field.mul(2n, k1));
    frame.setNextValue(0, nv0);
}

function incConstraint(frame: EvaluationFrame, field: FiniteField) {
    const v0 = frame.getValue(0);
    const k0 = frame.getConst(0);
    const k1 = frame.getConst(1);
    const nv0 = frame.getNextValue(0);

    return field.sub(nv0, field.add(field.add(field.add(v0, 1n), k0), field.mul(2n, k1)));
}

// TESTING
// ================================================================================================
let steps = 2**6, result = 292n;

const inputs = [1n];
const cst1: Constant = {
    values  : [1n, 2n, 3n, 4n],
    pattern : ConstantPattern.repeat
}

const cst2: Constant = {
    values  : [1n, 2n, 3n, 4n, 5n, 6n, 7n, 8n],
    pattern : ConstantPattern.stretch
};

const assertions = [{ step: 0, register: 0, value: 1n }, { step: steps-1, register: 0, value: result }];
const proof = incstark.prove(assertions, steps, inputs, [cst1, cst2]);
console.log('-'.repeat(100));
incstark.verify(assertions, proof, steps, [cst1, cst2]);
console.log('-'.repeat(100));