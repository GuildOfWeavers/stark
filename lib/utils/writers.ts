// IMPORTS
// ================================================================================================
import { BatchMerkleProof } from 'stark';
import { MAX_ARRAY_LENGTH } from './sizeof';

// PUBLIC FUNCTIONS
// ================================================================================================
export function writeBufferArray(buffer: Buffer, offset: number, array: Buffer[]) {

    // 1 byte for the array size (max 256 is written as 0)
    offset = buffer.writeUInt8(array.length === MAX_ARRAY_LENGTH ? 0 : array.length, offset);

    for (let i = 0; i < array.length; i++) {
        offset += array[i].copy(buffer, offset);
    }

    return offset;
}

export function writeMerkleProof(buffer: Buffer, offset: number, proof: BatchMerkleProof, nodeSize: number): number {
    offset = writeBigIntArray(buffer, offset, proof.values, nodeSize);
    offset = writeBigIntMatrix(buffer, offset, proof.nodes, nodeSize);
    offset = buffer.writeUInt8(proof.depth, offset);
    return offset;
}

export function writeBigIntArray(buffer: Buffer, offset: number, array: bigint[], elementSize: number): number {
    
    // 1 byte for the array size (max 256 written as 0)
    offset = buffer.writeUInt8(array.length === MAX_ARRAY_LENGTH ? 0 : array.length, offset);
    
    const hexSize = elementSize * 2;
    for (let i = 0; i < array.length; i++) {
        const hexValue = array[i].toString(16).padStart(hexSize, '0');
        offset += buffer.write(hexValue, offset, elementSize, 'hex');
    }

    return offset;
}

export function writeBigIntMatrix(buffer: Buffer, offset: number, matrix: bigint[][], elementSize: number): number {

    // 1 byte for the number of columns (max 256 written as 0)
    offset = buffer.writeUInt8(matrix.length === MAX_ARRAY_LENGTH ? 0 : matrix.length, offset);

    // then write lengths of each column (1 byte each, max 255)
    for (let i = 0; i < matrix.length; i++) {
        offset = buffer.writeUInt8(matrix[i].length, offset);
    }

    // then write the actual values
    const hexSize = elementSize * 2;
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            const hexValue = matrix[i][j].toString(16).padStart(hexSize, '0');
            offset += buffer.write(hexValue, offset, elementSize, 'hex');
        }
    }

    return offset;
}

export function writeBigInt(buffer: Buffer, offset: number, value: bigint, size: number): number {
    const hexValue = value.toString(16).padStart(size * 2, '0');
    offset += buffer.write(hexValue, offset, size, 'hex');
    return offset;
}