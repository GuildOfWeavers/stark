// IMPORTS
// ================================================================================================
import { HashAlgorithm, HashFunction } from 'stark';
import * as crypto from 'crypto';
import { sha256, digestSize as sha256DigestSize } from './hash/sha256';

// RE-EXPORTS
// ================================================================================================
export { readMerkleProof, readBigIntArray, readBigIntMatrix, readBigInt, readBufferArray } from './readers';
export { writeMerkleProof, writeBigIntArray, writeBigIntMatrix, writeBigInt, writeBufferArray } from './writers';
export { sizeOf } from './sizeof';
export { Logger } from './Logger';

// PUBLIC FUNCTIONS
// ================================================================================================
export function isPowerOf2(value: number | bigint): boolean {
    if (typeof value === 'bigint') {
        return (value !== 0n) && (value & (value - 1n)) === 0n;
    }
    else {
        return (value !== 0) && (value & (value - 1)) === 0;
    }
}

// PSEUDORANDOM
// ================================================================================================
export function getPseudorandomIndexes(seed: bigint, count: number, max: number, excludeMultiplesOf = 0): number[] {
    const modulus = BigInt(max);
    const skip = BigInt(excludeMultiplesOf);
    const indexes = new Set<bigint>();

    while (indexes.size < count) {
        seed = sha256(seed);
        let index = seed % modulus;
        if (skip && index % skip === 0n) continue;
        if (indexes.has(index)) continue;
        indexes.add(index)
    }

    const result: number[] = [];
    for (let index of indexes) {
        result.push(Number.parseInt(index.toString(16), 16));
    }

    return result;
}

export function getPseudorandomValues(seed: bigint, count: number): bigint[] {
    const values = new Array<bigint>(count);
    for (let i = 0; i < count; i++) {
        values[i] = seed = sha256(seed);
    }
    return values;
}

// HASHING
// ================================================================================================
export function hash(buffer: Buffer, algorithm: HashAlgorithm): bigint {
    let hash: crypto.Hash;
    switch (algorithm) {
        case HashAlgorithm.sha256: {
            hash = crypto.createHash('sha256');
            break;
        }
        default: {
            throw new TypeError('Invalid hash algorithm');
        }
    }

    hash.update(buffer);
    return BigInt('0x' + hash.digest().toString('hex'));
}

export function getHashFunction(algorithm: HashAlgorithm): HashFunction {
    switch (algorithm) {
        case HashAlgorithm.sha256: {
            return sha256;
        }
        default: {
            throw new TypeError('Invalid hash algorithm');
        }
    }
}

export function getHashDigestSize(algorithm: HashAlgorithm): number {
    switch (algorithm) {
        case HashAlgorithm.sha256: {
            return sha256DigestSize;
        }
        default: {
            throw new TypeError('Invalid hash algorithm');
        }
    }
}