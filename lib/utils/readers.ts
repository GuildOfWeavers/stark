// IMPORTS
// ================================================================================================
import { BatchMerkleProof } from 'stark';
import { MAX_ARRAY_LENGTH } from './sizeof';

// PUBLIC FUNCTIONS
// ================================================================================================
export function readBufferArray(buffer: Buffer, offset: number, elementSize: number) {

    const arrayLength = buffer.readUInt8(offset) || MAX_ARRAY_LENGTH;   // 0 means 256
    offset += 1;

    const values = new Array<Buffer>(arrayLength);
    for (let i = 0; i < arrayLength; i++, offset += elementSize) {
        values[i] = Buffer.allocUnsafe(elementSize);
        buffer.copy(values[i], 0, offset, offset + elementSize);
    }

    return { values, offset };
}

export function readMerkleProof(buffer: Buffer, offset: number, nodeSize: number) {

    const valuesInfo = readBigIntArray(buffer, offset, nodeSize);
    offset = valuesInfo.offset;
    const nodesInfo = readBigIntMatrix(buffer, offset, nodeSize);
    offset = nodesInfo.offset;
    const depth = buffer.readUInt8(offset);
    offset += 1;

    const proof: BatchMerkleProof = {
        values  : valuesInfo.values,
        nodes   : nodesInfo.matrix,
        depth   : depth
    };

    return { proof, offset };
}

export function readBigIntArray(buffer: Buffer, offset: number, nodeSize: number) {

    const arrayLength = buffer.readUInt8(offset) || MAX_ARRAY_LENGTH;   // 0 means 256
    offset += 1;

    const values = new Array<bigint>(arrayLength);
    for (let i = 0; i < arrayLength; i++, offset += nodeSize) {
        const hexValue = buffer.toString('hex', offset, offset + nodeSize);
        values[i] = BigInt('0x' + hexValue);
    }

    return { values, offset };
}

export function readBigIntMatrix(buffer: Buffer, offset: number, elementSize: number) {

    const columnCount = buffer.readUInt8(offset) || MAX_ARRAY_LENGTH;   // 0 means 256
    offset += 1;

    const matrix = new Array<bigint[]>(columnCount);
    for (let i = 0; i < columnCount; i++, offset += 1) {
        matrix[i] = new Array<bigint>(buffer.readUInt8(offset));
    }

    for (let i = 0; i < columnCount; i++) {
        let column = matrix[i];
        for (let j = 0; j < column.length; j++, offset += elementSize) {
            column[j] = readBigInt(buffer, offset, elementSize);
        }
    }

    return { matrix, offset };
}

export function readBigInt(buffer: Buffer, offset: number, size: number) {
    const hexValue = buffer.toString('hex', offset, offset + size);
    return BigInt('0x' + hexValue);
}