const arrayLength = 1000000;

const numbers = new Array<number>(arrayLength);
const bigints = new Array<bigint>(arrayLength);
const buffers = new Array<Buffer>(arrayLength);
const ab = new ArrayBuffer(arrayLength * 32);

let start = Date.now();
for (let i = 0; i < arrayLength; i++) {
    numbers[i] = i;
}
console.log(`Number array populated in ${Date.now() - start} ms`);

start = Date.now();
for (let i = 0; i < arrayLength; i++) {
    bigints[i] = BigInt(i);
}
console.log(`Bigint array populated in ${Date.now() - start} ms`);

start = Date.now();
for (let i = 0; i < arrayLength; i++) {
    buffers[i] = bigintToBuffer(bigints[i], 32);
}
console.log(`Converted to buffers in ${Date.now() - start} ms`);

const buf = Buffer.from(ab);
start = Date.now();
for (let i = 0, offset = 0; i < arrayLength; i++) {
    offset += buffers[i].copy(buf, offset);
}
console.log(`Wrote to buffer in ${Date.now() - start} ms`);

function bigintToBuffer(value: bigint, size: number): Buffer {
    const hexValue = value.toString(16).padStart(size * 2, '0');
    return Buffer.from(hexValue, 'hex');
}

/*
const samples = 10;
const size = 2**24;

function foo1(v: number[][], step: number) {
    v[0][step+1] = v[0][step] + 1;
    v[1][step+1] = v[1][step] + 2;
    v[2][step+1] = v[2][step] + 3;
}

function foo2(v: number[][], step: number) {
    return [
        v[0][step] + 1,
        v[1][step] + 2,
        v[2][step] + 3
    ];
}


let start: number, time = 0, v1 = [new Array<number>(size), new Array<number>(size), new Array<number>(size)];
for (let j = 0; j < samples; j++) {
    v1[0][0] = 1;
    v1[1][0] = 2;
    v1[2][0] = 3;
    start = Date.now();
    for (let i = 0; i < v1[0].length - 1; i++) {
        foo1(v1, i);
    }
    time += (Date.now() - start);
}
console.log(`v1 done in ${time/samples} ms`);

time = 0;
let v2 = [new Array<number>(size), new Array<number>(size), new Array<number>(size)];
for (let j = 0; j < samples; j++) {
    v2[0][0] = 1;
    v2[1][0] = 2;
    v2[2][0] = 3;
    start = Date.now();
    for (let i = 0; i < v2[0].length - 1; i++) {
        let v = foo2(v2, i);
        for (let k = 0; k < v2.length; k++) {
            v2[k][i + 1] = v[k];
        }
    }
    time += (Date.now() - start);
}
console.log(`v2 done in ${time/samples} ms`);


/*
import { PrimeField } from '../index';
import { PeriodicRegister } from '../lib/frames/PeriodicRegister';
import { GapRegister } from '../lib/frames/GapRegister';

const extensionFactor = 8;
const modulus = 2n ** 256n - 351n * 2n ** 32n + 1n;
const F = new PrimeField(modulus);
const steps = 2**13;

const G2 = F.getRootOfUnity(steps * extensionFactor);
const domain2 = F.getPowerCycle(G2);

const constants = new Array<bigint>(64);
for (let i = 0; i < constants.length; i++) {
    constants[i] = BigInt(i);
}

const skip = BigInt(steps / constants.length);

const G3 = F.exp(G2, BigInt(extensionFactor) * skip);
const domain3 = F.getPowerCycle(G3);
const poly = F.interpolateRoots(domain3, constants);

const values = new Array<bigint>(domain2.length);
for (let i = 0; i < domain2.length; i++) {
    let x = domain2[i];
    let g = F.exp(x, skip);
    values[i] = F.evalPolyAt(poly, g);
}

const secrets = Array<bigint>(512);
for (let i = 0; i < secrets.length; i++) {
    secrets[i] = BigInt(i);
}

const G4 = F.exp(G2, BigInt(steps / secrets.length * extensionFactor));
const domain4 = F.getPowerCycle(G4);
const poly2 = F.interpolateRoots(domain4, secrets);
const values2 = F.evalPolyAtRoots(poly2, domain2);

// --------------------------------------------------------------------------------------------
const r1 = new PeriodicRegister(constants, steps, extensionFactor, G2, F);
const r2 = new GapRegister(secrets, steps, extensionFactor, G2, F);

for (let i = 0; i < domain2.length; i++) {
    if (r2.getValue(i, domain2) !== r2.getValueAt(domain2[i])) {
        console.log('Error!');
        break;
    }
}

console.log('done!');
*/