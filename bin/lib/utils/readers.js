"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sizeof_1 = require("./sizeof");
// PUBLIC FUNCTIONS
// ================================================================================================
function readBufferArray(buffer, offset, elementSize) {
    const arrayLength = buffer.readUInt8(offset) || sizeof_1.MAX_ARRAY_LENGTH; // 0 means 256
    offset += 1;
    const values = new Array(arrayLength);
    for (let i = 0; i < arrayLength; i++, offset += elementSize) {
        values[i] = Buffer.allocUnsafe(elementSize);
        buffer.copy(values[i], 0, offset, offset + elementSize);
    }
    return { values, offset };
}
exports.readBufferArray = readBufferArray;
function readMerkleProof(buffer, offset, nodeSize) {
    const valuesInfo = readBigIntArray(buffer, offset, nodeSize);
    offset = valuesInfo.offset;
    const nodesInfo = readBigIntMatrix(buffer, offset, nodeSize);
    offset = nodesInfo.offset;
    const depth = buffer.readUInt8(offset);
    offset += 1;
    const proof = {
        values: valuesInfo.values,
        nodes: nodesInfo.matrix,
        depth: depth
    };
    return { proof, offset };
}
exports.readMerkleProof = readMerkleProof;
function readBigIntArray(buffer, offset, nodeSize) {
    const arrayLength = buffer.readUInt8(offset) || sizeof_1.MAX_ARRAY_LENGTH; // 0 means 256
    offset += 1;
    const values = new Array(arrayLength);
    for (let i = 0; i < arrayLength; i++, offset += nodeSize) {
        const hexValue = buffer.toString('hex', offset, offset + nodeSize);
        values[i] = BigInt('0x' + hexValue);
    }
    return { values, offset };
}
exports.readBigIntArray = readBigIntArray;
function readBigIntMatrix(buffer, offset, elementSize) {
    const columnCount = buffer.readUInt8(offset) || sizeof_1.MAX_ARRAY_LENGTH; // 0 means 256
    offset += 1;
    const matrix = new Array(columnCount);
    for (let i = 0; i < columnCount; i++, offset += 1) {
        matrix[i] = new Array(buffer.readUInt8(offset));
    }
    for (let i = 0; i < columnCount; i++) {
        let column = matrix[i];
        for (let j = 0; j < column.length; j++, offset += elementSize) {
            column[j] = readBigInt(buffer, offset, elementSize);
        }
    }
    return { matrix, offset };
}
exports.readBigIntMatrix = readBigIntMatrix;
function readBigInt(buffer, offset, size) {
    const hexValue = buffer.toString('hex', offset, offset + size);
    return BigInt('0x' + hexValue);
}
exports.readBigInt = readBigInt;
//# sourceMappingURL=readers.js.map