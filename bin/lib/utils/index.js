"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crypto = require("crypto");
const sha256_1 = require("./hash/sha256");
// RE-EXPORTS
// ================================================================================================
var readers_1 = require("./readers");
exports.readMerkleProof = readers_1.readMerkleProof;
exports.readBigIntArray = readers_1.readBigIntArray;
exports.readBigIntMatrix = readers_1.readBigIntMatrix;
exports.readBigInt = readers_1.readBigInt;
exports.readBufferArray = readers_1.readBufferArray;
var writers_1 = require("./writers");
exports.writeMerkleProof = writers_1.writeMerkleProof;
exports.writeBigIntArray = writers_1.writeBigIntArray;
exports.writeBigIntMatrix = writers_1.writeBigIntMatrix;
exports.writeBigInt = writers_1.writeBigInt;
exports.writeBufferArray = writers_1.writeBufferArray;
var sizeof_1 = require("./sizeof");
exports.sizeOf = sizeof_1.sizeOf;
var Logger_1 = require("./Logger");
exports.Logger = Logger_1.Logger;
// PUBLIC FUNCTIONS
// ================================================================================================
function isPowerOf2(value) {
    if (typeof value === 'bigint') {
        return (value !== 0n) && (value & (value - 1n)) === 0n;
    }
    else {
        return (value !== 0) && (value & (value - 1)) === 0;
    }
}
exports.isPowerOf2 = isPowerOf2;
// PSEUDORANDOM
// ================================================================================================
function getPseudorandomIndexes(seed, count, max, excludeMultiplesOf = 0) {
    const modulus = BigInt(max);
    const skip = BigInt(excludeMultiplesOf);
    const indexes = new Set();
    while (indexes.size < count) {
        seed = sha256_1.sha256(seed);
        let index = seed % modulus;
        if (skip && index % skip === 0n)
            continue;
        if (indexes.has(index))
            continue;
        indexes.add(index);
    }
    const result = [];
    for (let index of indexes) {
        result.push(Number.parseInt(index.toString(16), 16));
    }
    return result;
}
exports.getPseudorandomIndexes = getPseudorandomIndexes;
function getPseudorandomValues(seed, count) {
    const values = new Array(count);
    for (let i = 0; i < count; i++) {
        values[i] = seed = sha256_1.sha256(seed);
    }
    return values;
}
exports.getPseudorandomValues = getPseudorandomValues;
// HASHING
// ================================================================================================
function hash(buffer, algorithm) {
    let hash;
    switch (algorithm) {
        case 1 /* sha256 */: {
            hash = crypto.createHash('sha256');
            break;
        }
        default: {
            throw new TypeError('Invalid hash algorithm');
        }
    }
    hash.update(buffer);
    return BigInt('0x' + hash.digest().toString('hex'));
}
exports.hash = hash;
function getHashFunction(algorithm) {
    switch (algorithm) {
        case 1 /* sha256 */: {
            return sha256_1.sha256;
        }
        default: {
            throw new TypeError('Invalid hash algorithm');
        }
    }
}
exports.getHashFunction = getHashFunction;
function getHashDigestSize(algorithm) {
    switch (algorithm) {
        case 1 /* sha256 */: {
            return sha256_1.digestSize;
        }
        default: {
            throw new TypeError('Invalid hash algorithm');
        }
    }
}
exports.getHashDigestSize = getHashDigestSize;
//# sourceMappingURL=index.js.map