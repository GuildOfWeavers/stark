"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sizeof_1 = require("./sizeof");
// PUBLIC FUNCTIONS
// ================================================================================================
function writeBufferArray(buffer, offset, array) {
    // 1 byte for the array size (max 256 is written as 0)
    offset = buffer.writeUInt8(array.length === sizeof_1.MAX_ARRAY_LENGTH ? 0 : array.length, offset);
    for (let i = 0; i < array.length; i++) {
        offset += array[i].copy(buffer, offset);
    }
    return offset;
}
exports.writeBufferArray = writeBufferArray;
function writeMerkleProof(buffer, offset, proof, nodeSize) {
    offset = writeBigIntArray(buffer, offset, proof.values, nodeSize);
    offset = writeBigIntMatrix(buffer, offset, proof.nodes, nodeSize);
    offset = buffer.writeUInt8(proof.depth, offset);
    return offset;
}
exports.writeMerkleProof = writeMerkleProof;
function writeBigIntArray(buffer, offset, array, elementSize) {
    // 1 byte for the array size (max 256 written as 0)
    offset = buffer.writeUInt8(array.length === sizeof_1.MAX_ARRAY_LENGTH ? 0 : array.length, offset);
    const hexSize = elementSize * 2;
    for (let i = 0; i < array.length; i++) {
        const hexValue = array[i].toString(16).padStart(hexSize, '0');
        offset += buffer.write(hexValue, offset, elementSize, 'hex');
    }
    return offset;
}
exports.writeBigIntArray = writeBigIntArray;
function writeBigIntMatrix(buffer, offset, matrix, elementSize) {
    // 1 byte for the number of columns (max 256 written as 0)
    offset = buffer.writeUInt8(matrix.length === sizeof_1.MAX_ARRAY_LENGTH ? 0 : matrix.length, offset);
    // then write lengths of each column (1 byte each, max 255)
    for (let i = 0; i < matrix.length; i++) {
        offset = buffer.writeUInt8(matrix[i].length, offset);
    }
    // then write the actual values
    const hexSize = elementSize * 2;
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            const hexValue = matrix[i][j].toString(16).padStart(hexSize, '0');
            offset += buffer.write(hexValue, offset, elementSize, 'hex');
        }
    }
    return offset;
}
exports.writeBigIntMatrix = writeBigIntMatrix;
function writeBigInt(buffer, offset, value, size) {
    const hexValue = value.toString(16).padStart(size * 2, '0');
    offset += buffer.write(hexValue, offset, size, 'hex');
    return offset;
}
exports.writeBigInt = writeBigInt;
//# sourceMappingURL=writers.js.map