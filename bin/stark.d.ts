declare module 'stark' {

    export type Polynom = bigint[];

    export const enum HashAlgorithm {
        sha256 = 1
    }

    export interface HashFunction {
        (v1: bigint, v2?: bigint): bigint;
    }

    export interface Logger {
        start(message?: string) : symbol;
        log(label: symbol, message: string): void;
        done(label: symbol, message?: string): void;
    }

    // FINITE FIELDS
    // --------------------------------------------------------------------------------------------
    export interface FiniteField {

        readonly characteristic  : bigint;
        readonly extensionDegree : number;
        readonly elementSize     : number;

        add(x: bigint, y: bigint): bigint;
        sub(x: bigint, y: bigint): bigint;
        mul(x: bigint, y: bigint): bigint;
        div(x: bigint, y: bigint): bigint;
        exp(x: bigint, y: bigint): bigint;
        inv(value: bigint): bigint;

        invMany(values: bigint[]): bigint[];
        mulMany(values: bigint[][], m1: bigint[], m2?: bigint[]): bigint[][];

        combine(values: bigint[], coefficients: bigint[]): bigint;
        combineMany(values: bigint[][], coefficients: bigint[]): bigint[];

        getPowerSeries(seed: bigint, length: number): bigint[];
        
        rand(): bigint;

        getRootOfUnity(order: number): bigint;
        getPowerCycle(rootOfUnity: bigint): bigint[];

        addPolys(a: Polynom, b: Polynom): Polynom;
        subPolys(a: Polynom, b: Polynom): Polynom;
        mulPolys(a: Polynom, b: Polynom): Polynom;
        divPolys(a: Polynom, b: Polynom): Polynom;

        mulPolyByConstant(p: Polynom, c: bigint): Polynom;

        evalPolyAt(p: Polynom, x: bigint): bigint;
        evalPolyAtRoots(p: Polynom, rootsOfUnity: bigint[]): bigint[];

        interpolate(xs: bigint[], ys: bigint[]): Polynom;
        interpolateRoots(rootsOfUnity: bigint[], ys: bigint[]): Polynom;
        interpolateQuarticBatch(xSets: bigint[][], ySets: bigint[][]): Polynom[];
    }

    export interface PrimeField extends FiniteField {
        new(modulus: bigint): PrimeField;
        
        mod(value: bigint): bigint;
    } 

    // MERKLE TREE
    // --------------------------------------------------------------------------------------------
    export interface BatchMerkleProof {
        values  : bigint[];
        nodes   : bigint[][];
        depth   : number;
    }
    
    export class MerkleTree {
        static create(values: bigint[], hashAlgorithm: HashAlgorithm): MerkleTree;
        static createAsync(values: bigint[], hashAlgorithm: HashAlgorithm): Promise<MerkleTree>;

        readonly root   : bigint;
        readonly values : bigint[];

        prove(index: number): bigint[];
        proveBatch(indexes: number[]): BatchMerkleProof;

        static verify(root: bigint, index: number, proof: bigint[], hashAlgorithm: HashAlgorithm): boolean;
        static verifyBatch(root: bigint, indexes: number[], proof: BatchMerkleProof, hashAlgorithm: HashAlgorithm): bigint[] | undefined;
    }

    // STARK
    // --------------------------------------------------------------------------------------------
    export interface StarkConfig {
        field               : FiniteField;
        registerCount       : number;
        constantCount?      : number;
        tFunction           : TransitionFunction;
        tConstraints        : TransitionConstraint[];
        tConstraintDegree   : number;
        extensionFactor?    : number;
        exeSpotCheckCount?  : number;
        friSpotCheckCount?  : number;

        hashAlgorithm?      : HashAlgorithm;
        logger?             : Logger;
    }

    export class Stark {
        constructor(config: StarkConfig);

        prove(assertions: Assertion[], steps: number, inputs: bigint[], constants?: Constant[]): StarkProof;
        verify(assertions: Assertion[], proof: StarkProof, steps: number, constants?: Constant[]): boolean;

        sizeOf(proof: StarkProof): number;
        serialize(proof: StarkProof): Buffer;
        parse(proof: Buffer): StarkProof;
    }

    export interface Constant {
        values  : bigint[];
        pattern : ConstantPattern;
    }

    export const enum ConstantPattern {
        repeat = 1, stretch = 2
    }

    export interface ExecutionFrame {
        getValue(index: number): bigint;
        getConst(index: number): bigint;

        setNextValue(index: number, value: bigint): void;
    }

    export interface EvaluationFrame {
        getValue(index: number): bigint;
        getConst(index: number): bigint;

        getNextValue(index: number): bigint;
    }

    export interface TransitionFunction {
        (frame: ExecutionFrame, field: FiniteField): void;
    }

    export interface TransitionConstraint {
        (frame: EvaluationFrame, field: FiniteField): bigint;
    }
    
    export interface Assertion {
        step    : number;
        register: number;
        value   : bigint;
    }

    export interface LowDegreeProof {
        components  : FriComponent[];
        remainder   : bigint[];
    }
    
    export interface FriComponent {
        columnRoot  : bigint;
        columnProof : BatchMerkleProof;
        polyProof   : BatchMerkleProof;
    }

    export interface StarkProof {
        evaluations: {
            root    : bigint;
            values  : Buffer[];
            nodes   : bigint[][];
            depth   : number;
            bpc     : number;
        };
        degree: {
            root    : bigint;
            lcProof : BatchMerkleProof;
            ldProof : LowDegreeProof;
        }
    }

    // INTERNAL
    // --------------------------------------------------------------------------------------------
    export interface EvaluationContext {
        field           : FiniteField;
        steps           : number;
        extensionFactor : number;
        rootOfUnity     : bigint;
        registerCount   : number;
        constantCount   : number;
    }

    export interface ReadonlyRegister {
        getValue(step: number, skip: boolean): bigint;
        getValueAt(x: bigint): bigint;
    }
}