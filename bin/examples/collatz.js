"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// IMPORTS
// ================================================================================================
const assert = require("assert");
const index_1 = require("../index");
// STARK DEFINITION
// ================================================================================================
const modulus = 2n ** 32n - 3n * 2n ** 25n + 1n;
const colstark = new index_1.Stark({
    field: new index_1.PrimeField(modulus),
    registerCount: 1,
    tFunction: colTransition,
    tConstraints: [colConstraint],
    tConstraintDegree: 1
});
const r2 = colstark.field.getRootOfUnity(2);
function colTransition(frame, field) {
    const v = frame.getValue(0);
    const vnOdd = field.add(field.mul(3n, v), 1n);
    const vnEven = field.div(v, 2n);
    const isOdd = field.div(field.sub(1n, field.exp(r2, v)), 2n);
    const isEven = field.sub(1n, isOdd);
    frame.setNextValue(0, field.add(field.mul(isOdd, vnOdd), field.mul(isEven, vnEven)));
}
function colConstraint(frame, field) {
    const v = frame.getValue(0);
    const vnOdd = field.add(field.mul(3n, v), 1n);
    const vnEven = field.div(v, 2n);
    const isOdd = field.div(field.sub(1n, field.exp(r2, v)), 2n);
    const isEven = field.sub(1n, isOdd);
    const nv0 = frame.getNextValue(0);
    return field.sub(nv0, field.add(field.mul(isOdd, vnOdd), field.mul(isEven, vnEven)));
}
// TESTING
// ================================================================================================
let steps = 256, startVal = 871n;
const inputs = [startVal];
const assertions = [{ step: 0, register: 0, value: startVal }, { step: 178, register: 0, value: 1n }];
let proof = colstark.prove(assertions, steps, inputs);
console.log('-'.repeat(100));
let start = Date.now();
const buf = colstark.serialize(proof);
console.log(`Proof serialized in ${Date.now() - start} ms`);
console.log(`Proof size: ${buf.byteLength} bytes`);
assert(buf.byteLength === colstark.sizeOf(proof));
console.log('-'.repeat(100));
start = Date.now();
proof = colstark.parse(buf);
console.log(`Proof parsed in ${Date.now() - start} ms`);
console.log('-'.repeat(100));
colstark.verify(assertions, proof, steps);
console.log('-'.repeat(100));
//# sourceMappingURL=collatz.js.map